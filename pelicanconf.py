#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Wikimedia'
SITENAME = 'Monsters of Law'
SITEURL = ''

PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'en'
THEME = 'theme'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
OUTPUT_PATH = "public/"
# Order by filename
ARTICLE_ORDER_BY = 'reversed-date'
# Use document-relative URLs when developing
RELATIVE_URLS = True
# Don't paginate
DEFAULT_PAGINATION = False
# Omit the Date field from articles that do not need it
DEFAULT_METADATA = {'date': '2017-11-05'}
# Don't create any pages besides index
ARTICLE_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
PAGE_SAVE_AS = ''

# Static pages setup
STATIC_SAVE_AS = '{path}'
STATIC_URL = '{path}'
STATIC_PATHS = [
    'images/',
    'extra/htaccess',
    'extra/robots.txt',
    'extra/favicon.ico',
    'files/',
]
EXTRA_PATH_METADATA = {
    'extra/htaccess': {'path': '.htaccess'},
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'files/': {'path': 'files/'},
    'images/': {'path': 'images/'},
}
