# Monsters of Law website

This repository contains all the assets and content for the Monsters of Law website, available at <https://monstersoflaw.brussels>. It's running on [Pelican](http://getpelican.com), a static site generator.

## Editing the site

Unlike Wordpress and other content management systems, this site doesn't have an admin area; the content is directly edited here in the repository. GitLab provides a great browser-based interface to allow for the site management, and here you'll find pointers on how to change and add content to the site.

### Editing an event

The process of editing an event is done through the GitLab interface, which allows you to edit the files inside the repository.

![Editing content](/doc/editing-content.gif)

* Go to the `/content/events` directory inside the **Repository - Files** menu option (or use [this direct link](https://gitlab.com/manufacturaind/monsters-of-law/tree/master/content/events))
* Click on the file that you want to edit. Do note that Gitlab doesn't display the file contents correctly here (metadata appears on a single line), this is not a problem.
* Click the **Edit** link on the top right.
* You'll be presented with a text editor in which you can edit your content at will. 
* When you're done and want to save your changes, hit the **Commit changes** button at the bottom of the page.
* The site should be updated in a few seconds with the newest changes.

Keep reading to understand how the event files are structured.

### Content structure

Each event is contained on a single text file. Here is an example, with the first event:

```markdown
Title: Beauty and the Beast... and the Memes!
Subtitle: Copyright exceptions under EU and national laws
Date: 2018-12-03 17:00
Location: The Office, rue d'Arlon 80, 1050 Brussels
RegisterURL: mailto:eupolicy@wikimedia.be
Guest: Prof. Eleonora Rosati
GuestURL: http://www.elawnora.com/
GuestBio: An Italian-qualified lawyer (avvocato) with experience in the area of copyright, trademarks, fashion and internet laws. An associate professor in intellectual property (IP) law at the University of Southampton, an Editor of the Journal of Intellectual Property Law & Practice (Oxford University Press), an IP Kat and a Door Tenant at specialist IP set 8 New Square in London.

Copyright exceptions have been partially harmonised at the EU level. But are Member States transposing them correctly? The new Copyright in the Digital Single Market Directive will impose the adoption of new exceptions. What freedom do Member States enjoy in shaping national rules?

Despite harmonisation efforts in relation to specific types or works and horizontally through the Information Society Directive, (...)
```

The structure is pretty simple:

* the file starts with the **metadata** of the event: title, date, location, etc., one property on each line.
* after two newlines, the **description** of the event starts. This description is written in the Markdown format, which allows for rich text specifications using plain text. See [this cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for a simple explanation of how it works.

As for the **metadata**, each field plays a specific role in how the event is displayed.

* `Title` and `Subtitle` should be self-explanatory.
* `Date` is the date and time of the event. It must be specified in `YYYY-MM-DD HH:MM` format, otherwise the site will break! This field also determines whether an event should appear in the _Upcoming events_ or _Past events_ section.
* `Location` is a string containing the full address of the event.
* `RegisterURL` is the link featured in the _Register_ button. It can be a regular link to EventBrite or similar, or an e-mail address (in which case you should use the `mailto:` prefix, like the example above.
* `Guest` is the name of the guest speaker.
* `GuestURL` is the full URL to the guest's own site. Be sure to include `http://` or `https://`.
* `GuestBio` is the short bio for this speaker.
* More guests can be specified with the `Guest2`/`Guest2URL`/`Guest2Bio`, `Guest3`/`Guest3URL`/`Guest3Bio` fields, and so on.

The event **description** can be written in Markdown with as much content as you want. 

For upcoming events, any text after the first paragraph (delimited by two line breaks) will be hidden inside the "Read more" link on the website. 

You can also click the _Soft wrap_ option on the top right to wrap the long lines, which helps in the editing process by doing away with the scrollbars.

### Adding new events

![Adding content](/doc/event-creating.gif)

For new events, you just need to create a new file. After going to the [events directory](https://gitlab.com/manufacturaind/monsters-of-law/tree/master/content/events) in the repository browser, do the following:

* Click the **+** button next to the directory name, and select **New file**.
* Set the file name to whatever you want that allows you to remember what it is. Do remember to use the `.md` suffix in filename (e.g. `new-event.md`, otherwise the site generator won't recognise it as an event.
* Create the event file from scratch or by copy-pasting the above example.
* The rest plays out just like the editing instructions above.

### Adding speaker information

Individual speakers in the **Guests** section are also specified using Markdown files, living in the `/content/guests/` directory ([direct link](https://gitlab.com/manufacturaind/monsters-of-law/tree/master/content/guests). Here's an example:

```markdown
Title: Eleonora Rosati
URL: http://www.elawnora.com/
LinkedinURL: https://www.linkedin.com/in/eleonorarosati/
TwitterURL: https://twitter.com/eLAWnora

An Italian-qualified lawyer (avvocato) with experience in the area of copyright, trademarks, fashion and internet laws. An associate professor in intellectual property (IP) law at the University of Southampton, an Editor of the Journal of Intellectual Property Law & Practice (Oxford University Press), an IP Kat and a Door Tenant at specialist IP set 8 New Square in London.
```

The metadata follows the event format. Note that `LinkedinURL` and `TwitterURL` are optional fields.

The speaker bio follows two lines after the last field, just like the event description. You can create links and other rich text formatting here.

### Adding speaker photos

In order to display each guest's picture on the Guests section, you need to upload it to the repository. 

* Ensure the picture is in JPG format, at least 150pixels in width and height, and in square format. It helps the file size if it's not much larger than this.
* Ensure the filename is the speaker name as defined in the `Title:` field, in lower case and with hyphens instead of spaces. So in the case of a guest named `Jane Doe`, the file must be named `jane-doe.jpg`.
* Head to the `/content/images/guests/` directory ([direct link](https://gitlab.com/manufacturaind/monsters-of-law/tree/master/content/images/guests))
* Click the **+** icon next to the directory name, and select the **Upload file** option.
* You can drag the file to the box that appears, or select **Click to Upload** and browse to find your image.
* Hit the **Upload file** button.

If all the above requirements are met and there is a corresponding file for this guest in the `/content/guests/` directory as described above, the photo should appear next to their info in the "Guests" section on the site.


### Add photos to past event

For this you just need to add the image URLs to the "Slides" item in an event.

![Add photos](/doc/adding-slides.gif)

### Embed a Youtube video in a past event

Youtube videos can be added as a slide too.

![Add Youtube video](/doc/adding-youtube-video.gif)

### Formatting content in Past Events

The texts of Past Events can include formating such as headers, italics, bolds, links, bullet and ordered lists. The text is formated using [Markdown](https://en.wikipedia.org/wiki/Markdown).  
Here are some examples of how to do such formatting for quick reference.

```
## Header level 2
### Header level 3
#### Header level 4
##### Header level 5
###### Header level 6

**Bold** and _Italic_

[word with link](http://monstersoflaw.brussels/)

* this is a bullet list
* item
* item

1. this is an ordered list
2. item
3. item
```


### Add photos to past event

For this you just need to add the image URLs to the "Slides" item in an event.

![Add photos](/doc/adding-slides.gif)

### Embed a Youtube video in a past event

Youtube videos can be added as a slide too.

![Add Youtube video](/doc/adding-youtube-video.gif)

