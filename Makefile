PY?=python3
PELICAN?=pelican
PELICANOPTS=
LOAD_VENV_CMD=. `pwd`/.env/bin/activate

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif
RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

install:
	virtualenv .env --python=/usr/bin/python3
	$(LOAD_VENV_CMD); pip install -r requirements.txt

deploy: build
	git checkout master
	make publish
	ncftpput -z -R -f ftp-credentials.cfg / public/*

staging-deploy: build
	rsync -e ssh -P -rvzc --cvs-exclude public/ dh:~/www/files.manufacturaindependente.org/mol-preview/

staging-dry-deploy: build
	rsync -n -e ssh -P -rvzc --cvs-exclude public/ dh:~/www/files.manufacturaindependente.org/mol-preview/

build: 
	# the LC_TIME assignment ensures we use en_US locale by default for dates
	$(LOAD_VENV_CMD); LC_TIME="en_US.UTF-8" $(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
	./lib/sass theme/static/css/style.scss public/theme/css/style.css

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

regenerate:
	$(LOAD_VENV_CMD); $(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	@echo 'SERVER RUNNING AT http://localhost:$(PORT)'
	$(LOAD_VENV_CMD); cd $(OUTPUTDIR) && $(PY) -m pelican.server $(PORT)
else
	@echo 'SERVER RUNNING AT http://localhost:8000'
	$(LOAD_VENV_CMD); cd $(OUTPUTDIR) && $(PY) -m pelican.server
endif

publish: 
	$(LOAD_VENV_CMD); LC_TIME="en_US.UTF-8" $(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	./lib/sass theme/static/css/style.scss public/theme/css/style.css

.PHONY: install build help clean regenerate serve publish deploy
