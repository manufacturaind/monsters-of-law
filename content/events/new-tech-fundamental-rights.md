Title: New Technologies and Fundamental Rights
Subtitle: Maciej Szpunar <em>First Advocate General of the Court of Justice of the European Union</em>
Date: 2022-06-27 18:00
Location: TownHall Europe, Square de Meeûs 5-6, 1000 Brussels
RegisterURL: mailto:eupolicy@wikimedia.be
Guest: Maciej Szpunar
GuestURL: https://curia.europa.eu/jcms/jcms/rc4_170743/en
GuestBio: Maciej Szpunar, Professor of Law, has been Advocate General at the Court of Justice since 2013. He holds degrees in law from the University of Silesia and the College of Europe, Bruges; Doctor of Law; habilitated Doctor in Legal Science. <a href="#guests">Read more</a>
Slides: images/mol-11.webp
        images/mol-13.webp
        images/mol-16.webp
        images/mol-23.webp
        images/mol-25.webp
        images/mol-29.webp
        images/mol-33.webp
        images/mol-41.webp
        images/mol-81.webp


First Advocate General Maciej Szpunar talked about various aspects of the law of new technologies from the perspective of EU Law. It concerned in particular: injunctions against intermediaries, the concepts of communication to the public and territoriality, along with the changing nature of services and their “uberisation”.
