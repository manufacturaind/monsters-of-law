Title: Beauty and the Beast... and the Memes!
Subtitle: Copyright exceptions under EU and national laws
Date: 2018-12-03 17:00
Location: The Office, rue d'Arlon 80, 1050 Brussels
RegisterURL: mailto:eupolicy@wikimedia.be
Guest: Prof. Eleonora Rosati
GuestURL: http://www.elawnora.com/
GuestBio: An Italian-qualified lawyer (avvocato) with experience in the area of copyright, trademarks, fashion and internet laws. An associate professor in intellectual property (IP) law at the University of Southampton, an Editor of the Journal of Intellectual Property Law & Practice (Oxford University Press), an IP Kat and a Door Tenant at specialist IP set 8 New Square in London.
Slides: https://vimeo.com/305079055
        https://farm5.staticflickr.com/4887/45441063484_fca7ec34bb_z_d.jpg
        https://farm5.staticflickr.com/4886/44348051610_21ba03264e_z_d.jpg
        https://farm5.staticflickr.com/4820/46165274361_5158851663_z_d.jpg
                
Copyright exceptions have been partially harmonised at the EU level. But are Member States transposing them correctly? The new Copyright in the Digital Single Market Directive will impose the adoption of new exceptions. What freedom do Member States enjoy in shaping national rules?

Despite harmonisation efforts in relation to specific types or works and horizontally through the Information Society Directive, approaches at the national level have resulted in exceptions with varying scope across the EU. One example are exceptions that are limited to non-commercial uses, which lacks a corresponding limitation at the EU level. Are these national approaches to the implementation of EU directives legal? Despite the ‘fairy tale’ of the inherent flexibility of the EU copyright system, the answer appears to be in the negative. Through selected examples, this talk will explore the implications of questionable national transpositions and highlight what lessons, if any, can be learned to improve copyright law-making in Europe.

