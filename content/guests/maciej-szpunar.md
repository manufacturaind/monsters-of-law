Title: Maciej Szpunar
Date: 2020-03-16 17:00
URL: https://curia.europa.eu/jcms/jcms/rc4_170743/en
LinkedinURL:
TwitterURL:

Maciej Szpunar, Professor of Law, has been Advocate General at the Court of Justice since 2013. He holds degrees in law from the University of Silesia and the College of Europe, Bruges; Doctor of Law; habilitated Doctor in Legal Science. He was member of the Committee for Private International Law of the Civil Law Codification Commission under the Ministry of Justice in Poland, Undersecretary of State in the Office of the Committee for European Integration (2008-09), then in the Ministry of Foreign Affairs as well as Vice-Chairman of the Scientific Board of the Institute of Justice. He has been a member of the Board of Trustees of the Academy of European Law, Trier and a member of the Research Group on Existing EC Private Law (‘Acquis Group’). Maciej Szpunar is an author of numerous publications in the fields of European law and private international law.
