Title: Eleonora Rosati
Date: 2018-12-03 17:00
URL: http://www.elawnora.com/
LinkedinURL: https://www.linkedin.com/in/eleonorarosati/
TwitterURL: https://twitter.com/eLAWnora

An Italian-qualified lawyer (avvocato) with experience in the area of copyright, trademarks, fashion and internet laws. An associate professor in intellectual property (IP) law at the University of Southampton, an Editor of the Journal of Intellectual Property Law & Practice (Oxford University Press), an IP Kat and a Door Tenant at specialist IP set 8 New Square in London.
