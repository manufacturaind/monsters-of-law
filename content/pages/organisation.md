Title: Organisation

The Monsters of Law is organized by the [Free Knowledge Advocacy Group EU](https://meta.wikimedia.org/wiki/EU_policy/Statement_of_Intent), a group of Wikimedians who promote free access to, and re-use of, human knowledge. We take a stance on legislation on open access, copyright, and other major legislative and political changes affecting the [vision](https://meta.wikimedia.org/wiki/Vision), [mission](https://meta.wikimedia.org/wiki/Mission) and [values](https://meta.wikimedia.org/wiki/Values) of the [Wikimedia movement](https://meta.wikimedia.org/wiki/Wikimedia_movement).

