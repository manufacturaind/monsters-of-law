Title: Monsters of Law series

In the Monsters of Law series we ask academics and practitioners of law how the intersections of technology and law shape our everyday life. They answer with examples and perspectives that help us navigate the interesting times of technological transformation, both practically and from the policy perspective.
