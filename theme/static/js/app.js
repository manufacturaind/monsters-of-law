$(document).foundation();

$.expander.defaults.slicePoint = 10000;
$.expander.defaults.sliceOn = '</p>';

$('.collapsed').expander();

// fix Orbit instances inside accordions
// see https://github.com/zurb/foundation-sites/pull/3829
$(document).on('down.zf.accordion', function(ev) {
  $(window).resize();
});
